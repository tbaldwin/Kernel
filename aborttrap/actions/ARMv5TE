# Copyright (c) 2021, RISC OS Open Ltd
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met: 
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of RISC OS Open Ltd nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# 

# A8.6.66 LDRD (immediate)
LDRD_imm_A1(Rt,Rn,imm4H,imm4L,P,U,W)
{
	if(Rt==15)
		return aborttrap_ERROR(Unexpected); /* Don't allow some guy's dodgy code to cause us to access a nonexistant register! */
	return aborttrap_LDRD_imm(ctx,Rt,Rt+1,Rn,(imm4H<<4)|imm4L,P,U,!P || W);
}

# A8.6.67 LDRD (literal)
LDRD_lit_A1(Rt,imm4H,imm4L,U,nonstandard)
{
	if(nonstandard || (Rt==15))
		return aborttrap_ERROR(Unexpected);
	return aborttrap_LDRD_lit(ctx,Rt,Rt+1,(imm4H<<4)|imm4L,U);
}

# A8.6.68 LDRD (register)
LDRD_reg_A1(Rt,Rn,Rm,P,U,W,nonstandard)
{
	if(nonstandard || (Rt==15))
		return aborttrap_ERROR(Unexpected);
	return aborttrap_LDRD_reg(ctx,Rt,Rt+1,Rn,Rm,P,U,!P || W);
}

# A8.6.200 STRD (immediate)
STRD_imm_A1(Rt,Rn,imm4H,imm4L,P,U,W)
{
	if(Rt==15)
		return aborttrap_ERROR(Unexpected); /* Don't allow some guy's dodgy code to cause us to access a nonexistant register! */
	return aborttrap_STRD_imm(ctx,Rt,Rt+1,Rn,(imm4H<<4)|imm4L,P,U,!P || W);
}

# A8.6.201 STRD (register)
STRD_reg_A1(Rt,Rn,Rm,P,U,W,nonstandard)
{
	if(nonstandard || (Rt==15))
		return aborttrap_ERROR(Unexpected);
	return aborttrap_STRD_reg(ctx,Rt,Rt+1,Rn,Rm,P,U,!P || W);
}

