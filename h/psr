/*
 * Copyright (c) 2021, RISC OS Open Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of RISC OS Open Ltd nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef PSR_H
#define PSR_H

typedef enum {
	USR32 = 0x10,
	FIQ32 = 0x11,
	IRQ32 = 0x12,
	SVC32 = 0x13,
	MON32 = 0x16,
	ABT32 = 0x17,
	HYP32 = 0x1a,
	UND32 = 0x1b,
	SYS32 = 0x1f,
} eprocmode;

#define PSR_N		0x80000000
#define PSR_Z		0x40000000
#define PSR_C		0x20000000
#define PSR_V		0x10000000
#define PSR_Q		0x08000000
#define PSR_IT10	0x06000000
#define PSR_J		0x01000000
#define PSR_GE		0x000f0000
#define PSR_IT72	0x0000fc00
#define PSR_E		0x00000200
#define PSR_A		0x00000100
#define PSR_I		0x00000080
#define PSR_F		0x00000040
#define PSR_T		0x00000020
#define PSR_M		0x0000001f

/* PSR bits which define the instruction set */
#define PSR_ISET		(PSR_J | PSR_E)
#define PSR_ISET_ARM		0
#define PSR_ISET_THUMB		PSR_E
#define PSR_ISET_JAZELLE	PSR_J
#define PSR_ISET_THUMBEE	(PSR_J | PSR_E)

#define PSR26_N		0x80000000
#define PSR26_Z		0x40000000
#define PSR26_C		0x20000000
#define PSR26_V		0x10000000
#define PSR26_I		0x08000000
#define PSR26_F		0x04000000
#define PSR26_M		0x00000003

#endif
